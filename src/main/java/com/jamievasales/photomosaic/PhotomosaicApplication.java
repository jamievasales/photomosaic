package com.jamievasales.photomosaic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhotomosaicApplication {

    public static void main(String[] args) {
        SpringApplication.run(PhotomosaicApplication.class, args);
    }

}
