package com.jamievasales.photomosaic.util;

import com.jamievasales.photomosaic.image.MoviePoster;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageSplitter {
    private final Image sourceImage;
    private final int width;
    private final int height;

    ImageSplitter(BufferedImage sourceImage) {
        this.sourceImage = sourceImage;
        this.height = sourceImage.getHeight() / 3;
        this.width = sourceImage.getWidth() * 3;
    }

    public BufferedImage[] splitImage() {
        return null;
    }
}
