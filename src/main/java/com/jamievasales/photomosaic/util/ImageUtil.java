package com.jamievasales.photomosaic.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ImageUtil {
    public static BufferedImage imageFromFile(File file) {
        try {
            return ImageIO.read(file);
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }
    public static void copyImage(BufferedImage image, String destination, String name) {
        File file = new File(destination + name);
        try {
            ImageIO.write(image, "jpg", file);
        } catch (IOException ioException) {
            throw new IllegalStateException(ioException);
        }
    }

    public static void copyImage(File imageFile, String destination) {
        try {
            copyImage(ImageIO.read(imageFile), destination, imageFile.getName());
        } catch (IOException ioException) {
            throw new IllegalArgumentException(ioException.getMessage(),ioException);
        }
    }

    public static void cropAllToSquareAndMove(String dataSetPath, String copyPath) {
        List<File> files = Arrays.stream(Objects.requireNonNull(new File(dataSetPath).listFiles())).collect(Collectors.toList());
        for(File singleFile : files) {
            BufferedImage image = imageFromFile(singleFile);
            try {
                image = image.getSubimage(0,  0, image.getWidth(), image.getWidth());
            } catch (Exception e) {
                throw new IllegalStateException(image.getWidth() + " " + image.getHeight() + " " + singleFile.getName() + " " + e.getMessage(),e);
            }
            copyImage(image, copyPath, singleFile.getName());
        }
    }

    public static BufferedImage getScaledInstance(BufferedImage img, int targetWidth,int targetHeight, Object hint, boolean higherQuality) {
        int type =
                (img.getTransparency() == Transparency.OPAQUE)
                        ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage ret = (BufferedImage) img;
        int w, h;
        if (higherQuality)
        {
            // Use multi-step technique: start with original size, then
            // scale down in multiple passes with drawImage()
            // until the target size is reached
            w = img.getWidth();
            h = img.getHeight();
        }
        else
        {
            // Use one-step technique: scale directly from original
            // size to target size with a single drawImage() call
            w = targetWidth;
            h = targetHeight;
        }

        do
        {
            if (higherQuality && w > targetWidth)
            {
                w /= 2;
                if (w < targetWidth)
                {
                    w = targetWidth;
                }
            }

            if (higherQuality && h > targetHeight)
            {
                h /= 2;
                if (h < targetHeight)
                {
                    h = targetHeight;
                }
            }

            BufferedImage tmp = new BufferedImage(w, h, type);
            Graphics2D g2 = tmp.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
            g2.drawImage(ret, 0, 0, w, h, null);
            g2.dispose();

            ret = tmp;
        } while (w != targetWidth || h != targetHeight);

        return ret;
    }
}
