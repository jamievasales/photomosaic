package com.jamievasales.photomosaic.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.jamievasales.photomosaic.util.ImageUtil.*;
import static org.junit.jupiter.api.Assertions.*;

class ImageSplitterTest {
    BufferedImage sourceImage;
    ImageSplitter imageSplitter;

    @BeforeEach
    void setUp() throws IOException {
        //File file = new File("/tmp/sourceImage.jpg");
        File file = new File("/tmp/jamie.jpg");
        sourceImage = ImageIO.read(file);
       // imageSplitter = new ImageSplitter(sourceImage);
    }

    @Test
    public void ImageSplitterShouldReturnNumberOfRowsNeeded() throws IOException {
        File file = new File("/tmp/sourceImage.jpg");
            sourceImage = ImageIO.read(file);
            BufferedImage scaledImage = getScaledInstance(sourceImage, (int) (sourceImage.getWidth() * 1.5), (int) (sourceImage.getHeight() * 1.5), RenderingHints.VALUE_INTERPOLATION_BILINEAR, true);
            copyImage(scaledImage, "/tmp/resize-50/", file.getName());
    }
}